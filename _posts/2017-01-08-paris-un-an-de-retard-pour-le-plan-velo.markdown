---
layout: post
title:  "Paris : un an de retard pour le plan vélo"
date:   2017-01-11 17:06:00 +0100
categories: presse
author: Matthieu Quiret, Les Echos
link: http://www.lesechos.fr/politique-societe/regions/0211681908517-paris-un-an-de-retard-pour-le-plan-velo-2056347.php
image: assets/images/articles/paris-un-an-de-retard-pour-le-plan-velo.jpg
---

[![capture]({{ page.image | relative_url }})]({{ page.link }}){:rel='nofollow'}

Lire cet [article]({{ page.link }}){:rel='nofollow'}.
