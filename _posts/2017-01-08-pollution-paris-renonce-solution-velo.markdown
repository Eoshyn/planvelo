---
layout: post
title:  "En plein pic de pollution, Paris tourne le dos à la Solution Vélo"
date:   2016-12-06 00:42:00 +0100
author: Charles Maguin
categories: article
link: http://www.parisenselle.fr/pollution-paris-renonce-solution-velo/
image: assets/images/articles/pollution-paris-renonce-solution-velo.jpg
---

[![capture]({{ page.image | relative_url }})]({{ page.link }})

Nous vous informions des décisions prises en contradiction avec les annonces faites.

Lire cet [article]({{ page.link }}).
