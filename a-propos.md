---
layout: page
title: À propos
description: À propos de l'observatoire du plan vélo de Paris
permalink: /a-propos/
image: assets/images/pages/a-propos_carte-schema_directeur_2015-2020.jpeg
---

La maire de Paris a fait du vélo l'un de ses engagements phares pour la mandature 2014-2020. Votre association, [Paris en Selle](http://www.parisenselle.fr), suit les avancées, fait des propositions, et souhaite vous tenir informé(e)s en temps réel. C'est pourquoi nous avons créé cet observatoire qui vous aidera à vous faire votre propre opinion.

En gris, vous retrouverez tous les projets d'aménagement cyclable annoncés par la ville. Les traits plus épais correspondent aux axes structurants du Réseau Express Vélo (REVe). Pour en savoir plus, nous vous invitons à consulter la rubrique "[Les engagements]({{ site.baseurl }}{% link engagements.md %})".

[![carte_officielle]({{ page.image | relative_url }})]({{ page.image | relative_url }})

Notre [carte]({{ site.baseurl }}{% link index.md %}) ne comporte que les pointillés de la carte ci-dessus, et donc, pas ce que la mairie considère comme déja réalisé. Elle sera régulièrement mise à jour et nous espérons que les parties en vert, qui représentent les segments réalisés et de bonne qualité, deviennent de plus en plus nombreuses. A contrario, nous espérons user du rouge (projet abandonné) et du orange (projet réalisé et non satisfaisant) avec parcimonie.

Dans "[Actualités]({{ site.baseurl }}{% link articles.md %})", nous ne manquerons de vous tenir informé(e)s sur l'avancée des projets en fonction des informations qui nous parviendront au cours des comités vélos ou autres réunions publiques auxquelles participent nos membres. C'est pourquoi, pour faire avancer le vélo à Paris, nous avons besoin de vous!

Vous pouvez nous aider à faire avancer le vélo (et le plan vélo) à Paris en [adhérant](http://www.parisenselle.fr/association-velo/) et/ou en faisant un [don](http://www.parisenselle.fr/faire-un-don/). Si vous souhaitez recevoir nos informations, vous pouvez également vous inscrire à notre [newsletter](http://www.parisenselle.fr/newsletter/), ou nous suivre sur les réseaux sociaux, sur [Twitter](https://twitter.com/parisenselle){:rel='nofollow'} ou [Facebook](https://www.facebook.com/parisenselle){:rel='nofollow'}.
