---
layout: page
title: Les engagements
description: Les engagements pris par la mairie de Paris pour le plan vélo 2015-2020
permalink: /engagements/
image: assets/images/pages/engagements_carte-officielle-umap.jpg
---

Le mardi 14 avril 2015, le Conseil de Paris a dévoilé son Plan Vélo 2015-2020. Ce plan prévoit un investissement de plus de 150 millions d'euros pour faire de Paris "La Capitale du vélo 2020". Le Réseau Express Vélo (REVe) prévoyant la création de voies entièrement dédiées aux vélos nous paraît l'un des engagements les plus fort et susceptible de faire croître le vélo de façon significative.

Depuis, cette volonté a été maintes fois réaffirmée dans les communiqués de La ville de Paris. C'est ici que vous pouvez les retrouver, à commencer par les Vœux 2017:

<iframe id="videoFrame" frameborder="0" src="//www.dailymotion.com/embed/video/x561m4u" allowfullscreen></iframe><br />

{% twitter https://twitter.com/C_Najdovski/status/817325377647480832 %}

{% twitter https://twitter.com/C_Najdovski/status/806876227185348608 %}
[![carte]({{ page.image | relative_url }})]({{ page.image | relative_url }})

Vous trouverez tous les détails du plan vélo annoncé sur le [site de la ville](http://www.paris.fr/actualites/paris-se-dote-d-un-nouveau-plan-velo-2255){:rel='nofollow'}.

Certains communiqués nous auraient échappés? N'hésitez pas à nous les faire parvenir.
